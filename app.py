from flask import Flask, render_template, request
import os
from werkzeug.utils import secure_filename

save_file = "/home/sagar/uploadFiles/upfs"

app = Flask(__name__)
#app.config['.'] = '.'

@app.route('/')
def upload_file1():
    return render_template('index.html')
    
@app.route('/uploader', methods = ['GET', 'POST'])
def uploaded():
    if request.method == 'POST':
        f = request.files['file']
        f.save(os.path.join(save_file, f.filename))
        return 'File uploaded'
    
if __name__ == '__main__':
    app.run(debug = True)
        
